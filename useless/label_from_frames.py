# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import cv2
import argparse
import json
import re

parser = argparse.ArgumentParser()
parser.add_argument('--video_name', type=str, required=True)
parser.add_argument('--n', type=int, default=0)
args = parser.parse_args()

n = input('Proceed with video {} ? enter y to continue : '.format(args.video_name))
if n != 'y':
    raise AssertionError('Wrong video statement')

PATH_PROJECT = os.getcwd()
PATH_DATA = os.path.join(PATH_PROJECT, 'data')
PATH_VIDEO = os.path.join(PATH_PROJECT, r'videos', args.video_name)
PATH_SAVE = os.path.join(PATH_DATA, args.video_name)
PATH_FRAMES = os.path.join(PATH_SAVE, 'frames')

if not os.path.exists(PATH_SAVE):
    os.mkdir(PATH_SAVE)

# Load Frames
frames = os.listdir(PATH_FRAMES)
try:
    frames.remove('.DS_Store')
except:
    pass

if args.n:
    frames = frames[args.n:]

for i, f in enumerate(frames):
    f = re.sub('frame', '', f)
    f = re.sub('s\.jpg', '', f)
    frames[i] = f

# Format json
data = {}
if os.path.exists(os.path.join(PATH_SAVE, 'labels.json')):
    with open(os.path.join(PATH_SAVE, 'labels.json'), 'r') as f:
        data = json.load(f)

# Mouse function
def select_point(event, x, y, flags, params):
    global point, point_selected
    if event == cv2.EVENT_LBUTTONDOWN:
        point = (x, y)
        point_selected = True

point_selected = False
point = ()

cv2.namedWindow('frame')
cv2.setMouseCallback('frame', select_point)

for i, t in enumerate(frames, 1):
    data[t] = [0, None, None]
    if i % 200 == 0:
        print('\n' + str(i + args.n) + '\n')
    path = PATH_FRAMES + '/frame' + t + 's.jpg'
    img = cv2.imread(path)
    cv2.imshow('frame', img)
    key = cv2.waitKey(1000)

    if key == ord('p'):
        point_selected = False
        cv2.waitKey(0)
        if point_selected:
            label = int(input('1 for enemy - 2 for ally : '))
            data[t] = [label, point]
            print(point)
            print(data[t])

        cv2.waitKey(0)
        if point_selected:
            data[t].append(point)
            print(point)
            print(data[t])
            with open(os.path.join(PATH_SAVE, 'labels.json'), 'w') as f:
                json.dump(data, f)

        point_selected = False
        cv2.waitKey(0)  # Make video pause
        if point_selected:
            label = int(input('1 for enemy - 2 for ally : '))
            data[t] += [label, point]
            print(point)
            print(data[t])

        cv2.waitKey(0)
        if point_selected:
            data[t].append(point)
            print(point)
            print(data[t])
            with open(os.path.join(PATH_SAVE, 'labels.json'), 'w') as f:
                json.dump(data, f)

cv2.destroyAllWindows()
with open(os.path.join(PATH_SAVE, 'labels.json'), 'w') as f:
    json.dump(data, f)

