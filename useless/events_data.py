# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import json
import re
import pandas as pd

PATH_PROJECT = os.getcwd()
PATH_DATA = os.path.join(PATH_PROJECT, "data")

PATH_EVENTS = os.path.join(os.path.dirname(PATH_PROJECT), "AvP - Ressources", '2v1 Corridor', "Annotations")
events = os.listdir(PATH_EVENTS)
PATH_SAVE = os.path.join(PATH_DATA, '2v1 Corridor')
if not os.path.exists(PATH_SAVE):
    os.mkdir(PATH_SAVE)

# First File
with open(PATH_EVENTS + '/Corridor global.xml', 'r') as f:
    data = f.read().split('\n')

file = {"interaction": {
    "time": {
    }
}
        }

for line in data:
    if bool(re.findall('time', line)):
        tps = line.split('time=')[-1]
        assert len(tps.split()) == 2
        tps = tps.split()[0]
        file['interaction']['time'][tps] = {}

        type = line.split('type=')[-1].split('>')[0]
        file['interaction']['time'][tps]["type"] = type

        if bool(re.findall("commentaire", line)):
            comment = line.split('"')[1]
            file['interaction']['time'][tps]["commentaire"] = comment
        elif bool(re.findall("by", line)):
            user = line.split('"')[1]
            file['interaction']['time'][tps]["by"] = user

with open(PATH_SAVE + '/Corridor global.json', 'w') as fj:
    json.dump(file, fj)

# Second File
df = pd.read_csv(PATH_EVENTS + '/Corridor Flo.txt', sep='\t', header=None)
df.columns = ['time', 'type', 'comment']

file = {"interaction": {
    "time": {
    }
}
        }

file["interaction"]["time"] = {tps: {"type": type, "commentaire": com} for tps, type, com in zip(df.time, df.type, df.comment)}

with open(PATH_SAVE + '/Corridor Flo.json', 'w') as fj:
    json.dump(file, fj)


# Third File

with open(PATH_EVENTS + '/Corridor Yann.xml', 'r') as f:
    data = f.read().split('\n')

file = {"interaction": {
    "time": {
    }
}
        }

for line in data:
    if bool(re.findall('time', line)) and bool(re.findall("commentaire", line)):
        tps = line.split('time=')[-1]
        assert len(tps.split()) == 2
        tps = tps.split()[0]
        file['interaction']['time'][tps] = {}

        type = line.split('type=')[-1].split('>')[0]
        file['interaction']['time'][tps]["type"] = type

        comment = line.split('”')[1]
        file['interaction']['time'][tps]["commentaire"] = comment


with open(PATH_SAVE + '/Corridor Yann.json', 'w') as fj:
    json.dump(file, fj)



# Fourth File

PATH_EVENTS = os.path.join(os.path.dirname(PATH_PROJECT), "AvP - Ressources", '2v1v1Truck', "Annotations")
events = os.listdir(PATH_EVENTS)
PATH_SAVE = os.path.join(PATH_DATA, '2v1v1Truck')
if not os.path.exists(PATH_SAVE):
    os.mkdir(PATH_SAVE)

with open(PATH_EVENTS + '/Truck Florian.xml', 'r') as f:
    data = f.read().split('\n')[404:]

file = {"interaction": {
    "time": {
    }
}
        }

for line in data:
    if bool(re.findall('time', line)) and bool(re.findall("commentaire", line)):
        tps = line.split('time=')[-1]
        assert len(tps.split()) == 2
        tps = tps.split()[0]
        file['interaction']['time'][tps] = {}
        type = line.split('type=')[-1].split('>')[0]
        file['interaction']['time'][tps]["type"] = type
        comment = line.split('"')[1]
        file['interaction']['time'][tps]["commentaire"] = comment

with open(PATH_SAVE + '/Truck Florian.json', 'w') as fj:
    json.dump(file, fj)


# Fifth File

with open(PATH_EVENTS + '/Truck global.xml', 'r') as f:
    data = f.read().split('\n')

file = {"interaction": {
    "time": {
    }
}
        }

for line in data:
    if bool(re.findall('time', line)):
        tps = line.split('time=')[-1]
        assert len(tps.split()) == 2
        tps = tps.split()[0]
        file['interaction']['time'][tps] = {}
        type = line.split('type=')[-1].split('>')[0]
        file['interaction']['time'][tps]["type"] = type
        if bool(re.findall("commentaire", line)):
            comment = line.split('"')[1]
            file['interaction']['time'][tps]["commentaire"] = comment
        elif bool(re.findall("by", line)):
            user = line.split('"')[1]
            file['interaction']['time'][tps]["by"] = user


with open(PATH_SAVE + '/Truck global.json', 'w') as fj:
    json.dump(file, fj)


# Last file

with open(PATH_EVENTS + '/Truck Yann.xml', 'r') as f:
    data = f.read().split('\n')[400:]

file = {"interaction": {
    "time": {
    }
}
        }

for line in data:
    if bool(re.findall('time', line)) and bool(re.findall("commentaire", line)):
        tps = line.split('time=')[-1]
        assert len(tps.split()) == 2
        tps = tps.split()[0]
        file['interaction']['time'][tps] = {}
        type = line.split('type=')[-1].split('>')[0]
        file['interaction']['time'][tps]["type"] = type
        comment = line.split('”')[1]
        file['interaction']['time'][tps]["commentaire"] = comment

with open(PATH_SAVE + '/Truck Yann.json', 'w') as fj:
    json.dump(file, fj)


