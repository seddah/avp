# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import cv2


cap = cv2.VideoCapture(0)  # cv2.VideoCapture(0) for webcam
frameRate = int(cap.get(cv2.CAP_PROP_FPS))  # Frame per second


# Mouse function
def select_point(event, x, y, flags, params):
    global point, point_selected
    if event == cv2.EVENT_LBUTTONDOWN:
        point = (x, y)
        point_selected = True


point_selected = False
point = ()

cv2.namedWindow('frame')
cv2.setMouseCallback('frame', select_point)

while True:

    ret, frame = cap.read()
    frameId = cap.get(cv2.CAP_PROP_POS_FRAMES)  # Frame ID
    # if frameId % int(frameRate/3) == 0:
    #    print(frameId)
    #    cv2.imshow('frame', frame)

    cv2.imshow('frame', frame)
    key = cv2.waitKey(100)

    if point_selected:
        print(point)
        point_selected = False

    if key == 27:
        print('Working')
        break
    elif key == ord("p"):
        cv2.waitKey(0)  # Make video pause


print('Out of loop')
cap.release()
cv2.destroyAllWindows()