# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import cv2
import numpy as np
import time


PATH_PROJECT = os.getcwd()
PATH_DATA = os.path.join(PATH_PROJECT, 'videos')
file_names = os.listdir(PATH_DATA)[1:]

# Example read images and write on it
path = '/Users/matthieufuteral-peter/Desktop/Python/image_scrapping/data/Nadal/304.jpg'
img = cv2.imread(path)
gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
shape = img.shape[1], img.shape[0]

# write sth
cv2.line(img, (0, 0), shape, color=(0, 255, 0), thickness=2)

cv2.imshow('Nadal gray', gray_img)
cv2.imshow('Nadal', img)
cv2.waitKey(0)
cv2.destroyAllWindows()



