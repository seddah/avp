#!/bin/bash
module load conda
source activate sosweet_1
cd /home/rioc/mfuteral/avp/images_reduction
python autoencoder.py --epoch 10 --batch_size 16 --lr 1e-5