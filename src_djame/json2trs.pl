#!/usr/bin/perl

use LWP::Simple;                # From CPAN
use JSON qw( decode_json encode_json);     # From CPAN
use Data::Dumper;               # Perl core module
use strict;                     # Good practice
use warnings;                   # Good practice

use utf8;
binmode(STDOUT, ":utf8");
binmode(STDIN, ":utf8");
binmode(STDERR, ":utf8");

my $filename = $ARGV[0] or die "no filename given";

open FICIN, "<$filename"  || die "Can't open $filename: $!\n";

my $whole_json = join('', <FICIN>);

#print $whole_json."\n";


my $decoded_json = decode_json( $whole_json );

#print Dumper($decoded_json->{'video_name'}->{'2017-07-13 12-44-06.flv'});

my $ptr_metadata = $decoded_json->{'video_name'}->{'2017-07-13 12-44-06.flv'}->{'metadata'};


#die Dumper($ptr_metadata);
foreach my $json_el (@{$ptr_metadata}){
	#print $json_el->{'text'}."\n" if (defined($json_el->{'text'}));
#	die;
}

print encode_json($decoded_json->{'video_name'}->{'2017-07-13 12-44-06.flv'})."\n";



close FICIN;




