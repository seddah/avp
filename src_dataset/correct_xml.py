import os
import re
import xmltodict
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--video_name", type=str, required=True)
parser.add_argument("--file_name", type=str, required=True)
args = parser.parse_args()

path = os.path.join(os.path.dirname(os.path.dirname(os.getcwd())), 'AvP - Ressources', args.video_name, 'Annotations', args.file_name)

with open(path, 'r') as f:
    data = f.read()

regex = r"(\d+)"
subst = '\"\g<0>\"'
data = re.sub(regex, subst, data)

data = re.sub('\>\<|\/\>\<', ' ', data)
data = re.sub(r'/interaction', r'/', data)
data = re.sub(r'”', r'"', data)

dataset = xmltodict.parse(data)

with open(path, 'w') as f:
    f.write(data)