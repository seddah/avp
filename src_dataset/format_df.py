# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import json
import argparse
from sklearn.model_selection import train_test_split


parser = argparse.ArgumentParser()
parser.add_argument('--rename_frames', type=int, default=0)
args = parser.parse_args()

PATH_PROJECT = os.path.dirname(os.getcwd())
PATH_DATA = os.path.join(PATH_PROJECT, 'data')
PATH_TRAIN = os.path.join(PATH_PROJECT, 'train_set')
PATH_DEV = os.path.join(PATH_PROJECT, 'dev_set')
PATH_TEST = os.path.join(PATH_PROJECT, 'test_set')
videos = os.listdir(PATH_DATA)
try:
    videos.remove('.DS_Store')
except:
    pass


def mkpath(path):
    if not os.path.exists(path):
        os.mkdir(path)
    pass


mkpath(PATH_TRAIN)
mkpath(PATH_DEV)
mkpath(PATH_TEST)


labels = {}
for i, vid in enumerate(videos, 1):
    with open(os.path.join(PATH_DATA, vid) + '/labels.json', 'r') as fj:
        data = json.load(fj)
    data = {'{}_'.format(i) + k: v for k, v in data.items()}
    labels.update(data)
    print(vid + ' done !')

    if args.rename_frames:
        path_frames = os.path.join(PATH_DATA, vid, 'frames')
        frames_name = os.listdir(path_frames)
        for fname in frames_name:
            os.rename(path_frames + '/' + fname, path_frames + '/{}_'.format(i) + fname)
        print('Rename frames done' + '\n')


def translate_coordinate(lab):
    '''
    Change labels coordinate from (x_top_left, y_top_left, x_bottom_right, y_bottom_right)
    to (x_center, y_center, height, width)
    :param lab: label in the dataset
    :return: -
    '''
    if lab[0]:
        n_box = len(lab) // 3
        lab_hw = []
        x_A, y_A, x_B, y_B = 0, 0, 0, 0

        for i in range(n_box):
            x_A = lab[3*i + 1][0]
            y_A = lab[3*i + 1][1]
            x_B = lab[3*i + 2][0]
            y_B = lab[3*i + 2][1]
            lab_hw += [lab[3*i], [int((x_A + x_B)/2), int((y_A + y_B)/2)], [x_B - x_A, y_B - y_A]]

        return lab_hw

    else:
        return lab


print('Start translating coordinates' + '\n')
labels = {k: translate_coordinate(lab) for k, lab in labels.items()}

print('Start shuffling')

train_keys, test_keys = train_test_split(list(labels.keys()), test_size=0.2)
print('Sample train = {} frames'.format(len(train_keys)))

split = int(len(test_keys)/2)
dev_keys = test_keys[:split]
test_keys = test_keys[split:]
print('Sample test = {} frames'.format(len(test_keys)))
print('Sample dev = {} frames'.format(len(dev_keys)))

train_set = {k: v for k, v in labels.items() if k in train_keys}
test_set = {k: v for k, v in labels.items() if k in test_keys}
dev_set = {k: v for k, v in labels.items() if k in dev_keys}

print('Start saving')
with open(os.path.join(PATH_TRAIN, 'train_set.json'), 'w') as fj:
    json.dump(train_set, fj)

with open(os.path.join(PATH_TEST, 'test_set.json'), 'w') as fj:
    json.dump(test_set, fj)

with open(os.path.join(PATH_DEV, 'dev_set.json'), 'w') as fj:
    json.dump(dev_set, fj)

