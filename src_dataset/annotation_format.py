# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import re
import argparse
import json
import xmltodict
from tqdm import tqdm


parser = argparse.ArgumentParser()
parser.add_argument('--video_name', type=str, required=True)
parser.add_argument('--video_folder', type=str, required=True)
parser.add_argument('--file_name', type=str, required=True)
parser.add_argument('--time_lag', type=float, default=0.0)
args = parser.parse_args()

subpath = os.path.dirname(os.path.dirname(os.getcwd()))
path = os.path.join(subpath, 'AvP - Ressources')
path = os.path.join(path, args.video_folder, "Annotations")
path_file = os.path.join(path, args.file_name)

if os.path.exists(os.path.join(os.path.dirname(os.getcwd()), 'dataset.json')):
    with open(os.path.join(os.path.dirname(os.getcwd()), 'dataset.json'), 'r') as fj:
        dataset = json.load(fj)
else:
    dataset = {}


if os.path.splitext(args.file_name)[-1] == '.txt':
    with open(path_file, 'r') as f:
        file = f.readlines()

    n_events = int(list(dataset[args.video_name].keys())[-1])
    for i, event in enumerate(file, 1):
        if 'radar' in args.file_name:
            time, comment, _ = re.sub(r'\n', '', event).split('\t')
            event_type = 'radar'
        else:
            time, event_type, comment = re.sub(r'\n', '', event).split('\t')
        
        dataset[args.video_name][str(n_events + i)] = {'speaker':'', 'startTime': str(float(time) - 0.5), 
                                                       'endTime': str(float(time) + 0.5), 'text': '', 
                                                       'type': event_type, 'comment': comment}

elif os.path.splitext(args.file_name)[-1] == '.xml':
    with open(path_file, 'r', encoding='utf-8') as f:
        file = xmltodict.parse(f.read())['top']['interaction']

    n_events = int(list(dataset[args.video_name].keys())[-1])
    for i, event in enumerate(file, 1):
        event_type, event_time, comment = list(event.values())
        if args.time_lag:
            time = str(float(event_time) + args.time_lag)

        dataset[args.video_name][str(n_events + i)] = {'speaker':'', 'startTime': str(float(time) - 1.0),
                                                       'endTime': str(float(time) + 1.0), 'text': '',
                                                       'type': event_type, 'comment': comment}

with open(os.path.join(os.path.dirname(os.getcwd()), 'dataset.json'), 'w') as fj:
    json.dump(dataset, fj)







