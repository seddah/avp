# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import json
import time
import cv2
import argparse

PATH_PROJECT = os.path.dirname(os.getcwd())
PATH_DATA = os.path.join(PATH_PROJECT, 'data')

parser = argparse.ArgumentParser()
parser.add_argument('--video_name', type=str)
parser.add_argument('--check', type=int, default=0)
args = parser.parse_args()

PATH_VIDEO = os.path.join(PATH_DATA, args.video_name)
PATH_FRAMES = os.path.join(PATH_VIDEO, 'frames')
PATH_LABELS = os.path.join(PATH_VIDEO, 'labels.json')
frames = os.listdir(PATH_FRAMES)
try:
    frames.remove('.DS_Store')
except:
    pass

with open(PATH_LABELS, 'r') as fj:
    data = json.load(fj)


no_obj_frames = [t for t in data.keys() if data[t][0] == 0]
no_obj_ordered = [float(t) for t in no_obj_frames]
print('{} frames without objects'.format(len(no_obj_frames)))
incorrect_frames = []


time.sleep(3.0)
for i, t in enumerate(sorted(no_obj_ordered), 1):
    if i % 200 == 0:
        print(i)

    t = str(round(t, 4))
    n_figures = len(t.split('.')[-1])
    t += '0'*(4-n_figures)

    path = PATH_FRAMES + '/frame' + t + 's.jpg'
    img = cv2.imread(path)
    cv2.imshow('Image', img)
    key = cv2.waitKey(1000)
    if key == ord('d'):
        print('Frame {}s is added to the list of frames to delete'.format(t))
        incorrect_frames.append(t)
    elif key == ord('p'):
        print(i)
        cv2.waitKey(0)

print('\n'*2 + '{} frames incorrect'.format(len(incorrect_frames)) + '\n'*2)
print(incorrect_frames)

corrections = []
time.sleep(5.0)
if args.check:
    for t in incorrect_frames:
        path = PATH_FRAMES + '/frame' + t + 's.jpg'
        img = cv2.imread(path)
        cv2.imshow('Image', img)
        key = cv2.waitKey(2000)
        if key == ord('r'):
            corrections.append(t)
            print('Frame {}s is added to the correction list'.format(t))

    incorrect_frames = [t for t in incorrect_frames if t not in corrections]
    print('{} frames incorrect after checking'.format(len(incorrect_frames)))

k = input('Delete the frames above ? Enter y if so : ')
if k == 'y':
    for t in incorrect_frames:
        path = PATH_FRAMES + '/frame' + t + 's.jpg'
        os.remove(path)
    data = {t: v for t, v in data.items() if t not in incorrect_frames}
    with open(PATH_LABELS, 'w') as fj:
        json.dump(data, fj)
    print('Cleaning done')

cv2.destroyAllWindows()
