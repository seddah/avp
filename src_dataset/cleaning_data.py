# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import json
import re
import argparse

PATH_PROJECT = os.path.dirname(os.getcwd())
PATH_DATA = os.path.join(PATH_PROJECT, 'data')

parser = argparse.ArgumentParser()
parser.add_argument('--video_name', type=str)
args = parser.parse_args()

PATH_VIDEO = os.path.join(PATH_DATA, args.video_name)
PATH_FRAMES = os.path.join(PATH_VIDEO, 'frames')
PATH_LABELS = os.path.join(PATH_VIDEO, 'labels.json')
frames = os.listdir(PATH_FRAMES)
try:
    frames.remove('.DS_Store')
except:
    pass

with open(PATH_LABELS, 'r') as fj:
    data = json.load(fj)

times = [float(k) for k in data.keys()]
times = [str(round(t, 4)) for t in times]
for i, t in enumerate(times):
    n_figures = len(t.split('.')[-1])
    times[i] += '0'*(4-n_figures)
    assert len(times[i].split('.')[-1]) == 4

data = {times[i]: v for i, v in enumerate(data.values())}
i = 0
miss_keys = []
for k, v in data.items():
    if v[1] and v[1] == v[2]:
        os.remove(PATH_FRAMES + '/frame' + k + 's.jpg')
        miss_keys.append(k)
        i += 1
    if len(v) == 6 and v[4] == v[5] and os.path.exists(PATH_FRAMES + '/frame' + k + 's.jpg'):
        os.remove(PATH_FRAMES + '/frame' + k + 's.jpg')
        miss_keys.append(k)
        i += 1
    if len(v) == 6 and os.path.exists(PATH_FRAMES + '/frame' + k + 's.jpg'):
        if not v[0] or not v[3]:
            os.remove(PATH_FRAMES + '/frame' + k + 's.jpg')
            miss_keys.append(k)
            i += 1
print(i)
data = {k: v for k, v in data.items() if k not in miss_keys}

with open(PATH_LABELS, 'w') as fj:
    json.dump(data, fj)

miss_frames = 0
for f in frames:
    f = re.sub('frame', '', f)
    f = re.sub('s\.jpg', '', f)
    if f not in data.keys() and os.path.exists(PATH_FRAMES + '/frame' + k + 's.jpg'):
        miss_frames += 1
        os.remove(PATH_FRAMES + '/frame' + f + 's.jpg')

print("Miss Frames : {}".format(miss_frames))




