# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import argparse
from PIL import Image
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument('--video_name', type=str, required=True)
args = parser.parse_args()


subpath = os.path.dirname(os.getcwd())
path_data = os.path.join(subpath, 'data')
path_video = os.path.join(path_data, args.video_name)
path_frames = os.path.join(path_video, 'new_frames')

frames = os.listdir(path_frames)

def resize(img):
    img_path = os.path.join(path_frames, img)
    image = Image.open(img_path)
    new_img = image.resize((320, 180), Image.ANTIALIAS)
    new_img.save(img_path)
    pass


for img in tqdm(frames):
    resize(img)





