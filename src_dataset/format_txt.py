# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import re
import argparse
import json
import xmltodict

parser = argparse.ArgumentParser()
parser.add_argument('--video_folder', type=str, required=True)
parser.add_argument('--video_name', type=str, required=True)
args = parser.parse_args()


subpath = os.path.dirname(os.path.dirname(os.getcwd()))
path = os.path.join(subpath, 'AvP - Ressources')
path_text = os.path.join(path, args.video_folder, "Transcription Speech to Text")

if os.path.exists(os.path.join(os.path.dirname(os.getcwd()), 'dataset.json')):
    with open(os.path.join(os.path.dirname(os.getcwd()), 'dataset.json'), 'r') as fj:
        dataset = json.load(fj)
else:
    dataset = {}

txt_files = []
for f in os.listdir(path_text):
    if re.search('\.trs', f):
        txt_files.append(f)

if args.video_name == '2v1 Corridor.flv':
    time_lag = 7
else:
    time_lag = 0


events = {}
n_event = 0
for txt_file in txt_files:
    path_file = os.path.join(path_text, txt_file)
    if txt_file == '12 - 24 Corridor.trs':
        encoding = 'utf-8'
    else:
        encoding = 'iso8859_1'
    with open(path_file, 'r', encoding=encoding) as f:
        data = xmltodict.parse(f.read())['Trans']['Episode']['Section']['Turn']

    # Process text data
    for i, dt in enumerate(data):
        if '#text' in dt.keys():
            if len(dt['#text'].split('\n\n')) > 1:
                span_txt = dt['#text'].split('\n\n')
                for j, sync in enumerate(dt['Sync']):
                    if j == len(dt['Sync']) - 1:
                        end_time = dt['@endTime']
                    else:
                        end_time = dt['Sync'][j + 1]['@time']

                    if args.video_name == '2v1 Corridor.flv':
                        if dt['@speaker'] == 'spk1':
                            n_event += 1
                            events[n_event] = {"speaker": dt['@speaker'], "startTime": str(float(sync['@time']) + 7.0), 
                                               "endTime": str(float(end_time) + 7.0), "text": span_txt[j], "type": 0, "comment": ""}
                    else:
                        n_event += 1
                        events[n_event] = {"speaker": dt['@speaker'], "startTime": sync['@time'], 
                                           "endTime": end_time, "text": span_txt[j], "type": 0, "comment": ""}
            else:
                if args.video_name == '2v1 Corridor.flv':
                    if dt['@speaker'] == 'spk1':
                        n_event += 1
                        events[n_event] = {"speaker": dt['@speaker'], "startTime:": str(float(dt['@startTime']) + 7.0), 
                                           "endTime": str(float(dt["@endTime"]) + 7.0), "text": dt['#text'], "type": 0, "comment": ""}
                else:
                    n_event += 1
                    events[n_event] = {"speaker": dt['@speaker'], "startTime:": dt['@startTime'], 
                                       "endTime": dt["@endTime"], "text": dt['#text'], "type": 0, "comment": ""}

    print("{} processed".format(txt_file))


dataset[args.video_name] = events
with open(os.path.join(os.path.dirname(os.getcwd()), 'dataset.json'), 'w') as fj:
    json.dump(dataset, fj)




